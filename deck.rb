require_relative 'card'

class Deck
  ATTR_ARRAY = %i(deck suits ranks)
  attr_accessor *ATTR_ARRAY

  SUITS = %w(Spades Hearts Clubs Diamonds)
  RANKS = %w(2 3 4 5 6 7 8 9 10 Jack Queen King Ace)

  def initialize(suits, ranks)
    @deck = []
    @suits = suits
    @ranks = ranks
    create_deck
  end

  def shuffle
    @deck.shuffle!
  end

  def deal_card
    @deck.pop
  end

  def replace_with deck
    @suits = []
    @ranks = []
    @deck = deck
    deck.each { |card| add_suit_and_rank card }
    self
  end

  private

  def create_deck
    @suits.each do |suit|
      @ranks.each { |rank| @deck.push Card.new(suit, rank) }
    end
  end

  def add_suit_and_rank(card)
    suit = card.suit
    rank = card.rank
    @suits.push suit unless @suits.include?(suit)
    @ranks.push rank unless @ranks.include?(rank)
  end
end