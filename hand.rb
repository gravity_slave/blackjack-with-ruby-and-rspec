class Hand

  attr_accessor :dealt_cards
  VALUES = (2..10).to_a.reduce({}) {|hsh, int| hsh.merge({"#{int}" => int}) }.update('Jack' => 10, 'Queen' => 10, 'King' => 10, 'Ace' => 1)
  def initialize
    @dealt_cards = []
  end

  def add_card card
    @dealt_cards << card
  end

  def get_value
    card_ranks =  dealt_cards.collect { |card|  card.rank }
    value = card_ranks.map { |rank| VALUES[rank] }.inject(:+)
    if card_ranks.include?('Ace')
      value += 10 if value + 10 <= 21
    end
    value
  end

  def to_s
    report = dealt_cards.collect  { |card| card.show ? card.to_s.concat + ', ' : '' }.reduce('') { |str, val| str += val}
    if dealt_cards.first.show == false
      report = report + 'Total value: ' + (get_value - VALUES[dealt_cards.first.rank]).to_s
    else
      report = report + 'Total value: ' + get_value.to_s
    end
    report
  end
end