class Card
  ATTR_ARRAY = %i(suit rank show)
  attr_accessor *ATTR_ARRAY

  SUITS = %w(Spades Hearts Clubs Diamonds)
  RANKS = %w(2 3 4 5 6 7 8 9 10 Jack Queen King Ace)

  def initialize(suit,rank)
    @show = true
    @suit =  SUITS.include?(suit) ? suit : 'UNKNOWN'
    @rank = RANKS.include?(rank) ? rank  : 'UNKNOWN'
  end

  def to_s
   show ? %(#{rank} of #{suit}) : ''
  end
end